$(document).ready(function(){//로드영역
//로그인아이디 패스워드 입력시 서브밋 활성화
    var inputs4 = $(".logininput");

    $(inputs4).keyup(function() {
        ////값의 유무 여부 확인하기
        var is_empty_4 = false;

        $('#loginbox__form').find("input.logininput").each(function(){
            if(!$(this).val()) {
                is_empty_4 = true;
            }
        });

        if(is_empty_4) {//값이 비어있으면
            $(".loginbox__btn").attr('disabled', true);  //전송버튼 풀기
            $(".loginbox__btn").removeClass("is-active");//버튼은 활성화
            return;
        }else{
            $(".loginbox__btn").attr('disabled', false);  //전송버튼 풀기
            $(".loginbox__btn").addClass("is-active");//버튼은 활성화
        }
    });

});//로드영역
