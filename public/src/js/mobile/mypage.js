$(document).ready(function(){
    function mypageCheck(){
       var mypageAll = $('#mypageChkAll');
       //전체선택클릭시 전체선택
       $(document).on('click','#mypageChkAll',function(){
           if($(this).is(':checked')){
               $(".product__item .checkbox-only").prop("checked",true);
           }else{
               $(".product__item .checkbox-only").prop("checked",false);
           }
       });

       //하나해제시 전체선택해제
       $(document).on('click','.product__item .checkbox-only',function(){
           var productChkNum = $(".product__item .checkbox-only").length;
           if(mypageAll.is(':checked')){
               if($(".product__item .checkbox-only:checked").length == productChkNum){
                   $(".product__item .checkbox-only").prop("checked",true);
               }else{
                   mypageAll.prop("checked",false);
               }
           }
       });
       $(document).on('change','.product__item .checkbox-only, #mypageChkAll',function(){
           var isCheck = $('.product__item .checkbox-only:checked').length;
           if(isCheck != 0){
               $('.mypage-body').find('.button-v2').addClass('is-active').removeClass('is-disabled');
           }else{
               $('.mypage-body').find('.button-v2').removeClass('is-active').addClass('is-disabled');
           }
       });

       $(document).on('click','.js-mypage-remove',function(){
           $('.product__item .checkbox-only:checked').parents('.product__item').remove();
           var isCheck = $('.product__item .checkbox-only:checked').length;
		   var productNum = $('.mypage-body .product__item .checkbox-only').length;
           if(isCheck != 0){
               $('.mypage-body').find('.button-v2').addClass('is-active').removeClass('is-disabled');
           }else{
               $('.mypage-body').find('.button-v2').removeClass('is-active').addClass('is-disabled');
               $('#mypageChkAll').prop("checked",false);
           }
		   if(productNum == 0){
               $('.mypage-body').find('.tab-v2__num').eq(0).text('');
			   $('.no-message').removeClass('bld');
           }else{
               $('.mypage-body').find('.tab-v2__num').eq(0).text(productNum);
           }
       });
   }
    mypageCheck();
});
