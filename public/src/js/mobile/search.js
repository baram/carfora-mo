$(document).ready(function(){
    $('.js-category-open').on('click',function(e){
        e.preventDefault();
        $('.bottom-select.type-category').fadeIn(300,function(){
            $(this).find('.bottom-select__wrap').addClass('is-active')
        });
    });

    $('.js-category-input').on('click',function(e){
        e.preventDefault();
        $(this).parents('.bottom-select').fadeOut(500).find('.bottom-select__wrap').removeClass('is-active');
        var categoryName = $('.bottom-select.type-category .radio-v1:checked').val();
        $('.search-header__category').text(categoryName);
    });


    function numberWithCommas(x) {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }
    //레인지 슬라이더1
    function driveSlider(){
        var select1 = document.getElementById('drive-select1');

        // Append the option elements
        for (var i = 0; i <= 20; i++) {

            var option = document.createElement("option");
            var num = i;
            option.text = numberWithCommas(num) + '만 km';
            option.value = num*10000;

            select1.appendChild(option);
        }

        var select2 = document.getElementById('drive-select2');

        for (var i = 0; i <= 20; i++) {

            var option2 = document.createElement("option");
            var num = i;

            option2.text = numberWithCommas(num) + '만 km';
            option2.value = num*10000;

            select2.appendChild(option2);
        }

        var html5Slider = document.getElementById('drive-slider');
        var driveMin = -10000;
        var driveMax = 210000;
        noUiSlider.create(html5Slider, {
            start: [driveMin, driveMax],
            step: 10000,
            connect: true,
            range: {
                'min': driveMin,
                'max': driveMax
            }
        });
        html5Slider.noUiSlider.on('update', function (values, handle) {
            var value = values[handle];
            var value2 = html5Slider;
            if (handle) {
                if(Math.round(value) == driveMin){
                    select2.value = ""
                }else if(Math.round(value) == driveMax){
                    select2.value = ""
                }else{
                    select2.value = Math.round(value);
                }
            } else {
                if(Math.round(value) == driveMin){
                    select1.value = ""
                }else if(Math.round(value) == driveMax){
                    select1.value = ""
                }else{
                    select1.value = Math.round(value);
                }
            }
        });

        select1.addEventListener('change', function () {
            html5Slider.noUiSlider.set([this.value, null]);
        });

        select2.addEventListener('change', function () {
            html5Slider.noUiSlider.set([null, this.value]);
        });
    }

    function priceSlider(){
        var select1 = document.getElementById('price-select1');

        // Append the option elements
        for (var i = 0; i <= 100; i++) {
            var option = document.createElement("option");
            var num = i * 100;
            if(i == 100){
                option.text = '1억원';
                option.value = num;
                select1.appendChild(option);
            }else{
                option.text = numberWithCommas(num) + '만원';
                option.value = num;
                select1.appendChild(option);
            }

        }

        var select2 = document.getElementById('price-select2');

        for (var i = 0; i <= 100; i++) {

            var option2 = document.createElement("option");
            var num = i * 100;
            if(i == 100){
                option2.text = '1억원';
                option2.value = num;

                select2.appendChild(option2);
            }else{
                option2.text = numberWithCommas(num) + '만원';
                option2.value = num;

                select2.appendChild(option2);
            }

        }

        var html5Slider = document.getElementById('price-slider');
        var priceMin = -100;
        var priceMax = 10100;
        noUiSlider.create(html5Slider, {
            start: [priceMin, priceMax],
            connect: true,
            step:100,
            range: {
                'min': priceMin,
                'max': priceMax
            }
        });





        html5Slider.noUiSlider.on('update', function (values, handle) {

            var value = values[handle];

            if (handle) {
                if(Math.round(value) == priceMin){
                    select2.value = ""
                }else if(Math.round(value) == priceMax){
                    select2.value = ""
                }else{
                    select2.value = Math.round(value);
                }
            } else {
                if(Math.round(value) == priceMin){
                    select1.value = ""
                }else if(Math.round(value) == priceMax){
                    select1.value = ""
                }else{
                    select1.value = Math.round(value);
                }
            }
        });

        select1.addEventListener('change', function () {
            html5Slider.noUiSlider.set([this.value, null]);
        });

        select2.addEventListener('change', function () {
            html5Slider.noUiSlider.set([null, this.value]);
        });
    }
    priceSlider();
    driveSlider();

    $(".js-directly").on('click', function(e) {
        e.preventDefault();
        if ( $(this).parents('.search-menu__item').find('.period-style2').hasClass('is-directly') ) {
            $(this).parents('.search-menu__item').find('.period-style2').removeClass("is-directly");
        } else {
            $(this).parents('.search-menu__item').find('.period-style2').addClass("is-directly");
         }
    });
    $(".js-2depth-open").on('click', function() {
        var checkIs = $('.js-2depth-open').find('.tag__checkbox');
        if ( checkIs.prop('checked') ) {
            $(this).addClass('is-active')
        } else {
            $(this).removeClass('is-active')
         }
    });

    // 바디스크롤 막기
    var bodyY;
    function scrollStop(){
    	bodyY = $(window).scrollTop();
    	$('html, body').addClass("no-scroll");
    	$('.common').css({"top":-bodyY,'position':'relative'});
    }
    function scrollStart(){
    	$('html, body').removeClass("no-scroll");
    	$('.common').css({'top':'auto','position':'static'});
    	bodyY = $('html,body').scrollTop(bodyY);
    }

    $('.search-menu__link').on('click',function(e){
        e.preventDefault();
        var pageName = $(this).attr('data-page');
        var titName = $(this).parents('.search-menu__item').find('.search-menu__subject').text();
        $('.search-menu__tit').html(titName)
        $('.search-menu__list').addClass('is-hide');
        $(".type-"+pageName).addClass('is-active');
        $('.button-default.type-confirm').show();
        $('.button-default.type-apply').hide();
    })

    function searchMenuBack(){
        $('.search-menu__list').removeClass('is-hide');
        $('.tag').removeClass('is-active');
        $('.search-menu__tit').html('상세검색 <span class="color-point1">#Tag</span>');
        $('.button-default.type-confirm').hide();
        $('.button-default.type-apply').show();
    }
    $('.search-menu__back').on('click',function(e){
        e.preventDefault();
        if($('.search-menu__list').hasClass('is-hide')){
            searchMenuBack();
        }else{
            $('.search-menu, .bottom-search').removeClass('is-active');
            scrollStart()
        }
    });

    $('.hash__link.type-close').on('click',function(e){
        e.preventDefault();
        $(this).parents('.hash__item').remove();
    });

    $('.js-tag-open').on('click',function(e){
        e.preventDefault();
        $('.search-menu, .bottom-search').addClass('is-active');
        scrollStop();
    });

    $('.js-tag-close').on('click',function(e){
        e.preventDefault();
        $('.search-menu, .bottom-search').removeClass('is-active');
        scrollStart();
    });

    $('.js-tag-confirm').on('click',function(e){
        e.preventDefault();
        searchMenuBack();
    });


    //more search
    const moreOpen = document.querySelector('.js-more-open');
    const moreSearch = document.querySelector('.more-search');
    const moreClose = document.querySelector('.js-more-close');
    const searchInput = document.querySelector('.search_box__auto-input');
    try{
        moreOpen.addEventListener("click",function(e){
            e.preventDefault();
            searchInput.focus();
            moreSearch.classList.add('is-active');
        });
        moreClose.addEventListener("click",function(e){
            e.preventDefault();
            moreSearch.classList.remove('is-active');
        });
    }catch(e){}



    //자동완성
    var example = ["그랜저", "아이오닉","쏘나타","i30","스타렉스","코나"]
	 var $elem = $(".js-auto-input").autocomplete({
		// source:"my_car_keyword.php",
		source: example,
		position: {
		  my: "left top+4",
		  at: "left bottom",
		  collision: "none"
		},
		autoFocus: true,
		response: function response(event, ui) {},
		select: function select(event, ui) {//            console.log("Selected:" + ui.item.value);
		},
		focus: function focus(event, ui) {
		  return false;
		}
	  }),
		  elemAutocomplete = $elem.data("ui-autocomplete") || $elem.data("autocomplete");

	  if (elemAutocomplete) {
		elemAutocomplete._renderItem = function (ul, item) {
		  var newText = String(item.value).replace(new RegExp(this.term, "gi"), '<span style="color:#3897f0">$&</span>');
		  return $("<li></li>")
            .data("item.autocomplete", item)
    //            .prepend("")
            .append("<img src='../../img/mobile/mycar/logo_benz_m.png'class='ui-menu-item__img' ><div class=''><div class='t_wrap'>" + newText + "</div></div>")
            .appendTo(ul);
		};
	  }

	   var $elem2 = $(".js-auto-input2").autocomplete({
		// source:"my_car_keyword.php",
		source: example,
		position: {
		  my: "left top+4",
		  at: "left bottom",
		  collision: "none"
		},
		autoFocus: true,
		response: function response(event, ui) {},
		select: function select(event, ui) {//            console.log("Selected:" + ui.item.value);
		},
		focus: function focus(event, ui) {
		  return false;
		}
	  }),
		  elemAutocomplete = $elem2.data("ui-autocomplete") || $elem2.data("autocomplete");

	  if (elemAutocomplete) {
		elemAutocomplete._renderItem = function (ul, item) {
		  var newText = String(item.value).replace(new RegExp(this.term, "gi"), '<span style="color:#3897f0">$&</span>');
		  return $("<li></li>")
            .data("item.autocomplete", item)
    //            .prepend("")
            .append("<img src='../../img/mobile/mycar/logo_benz_m.png'class='ui-menu-item__img' ><div class=''><div class='t_wrap'>" + newText + "</div></div>")
            .appendTo(ul);
		};
	  }


});
