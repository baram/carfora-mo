$(document).ready(function(){
    $("input[name=purchase]").on('click', function() {
        $("#progress1").submit();
    });

    $('.js-history-back').on('click',function(){
        history.back();
    });


    //풉옵션 체크시 해제
    var checkOption = $(".js-one-check");
    var checkFulloption = $(".js-all-check");

    checkFulloption.click(function(){
        var gap = $(this).val();
        if(gap =! ""){//체크가 됬다면
            unlock_check(checkOption);
        }
    });
    checkOption.click(function(){
        var gap = $(this).val();
        if(gap =! ""){//체크가 됬다면
            unlock_check(checkFulloption);
        }
    });

    var acciAll = $(".js-all-acci");
    var acciOne = $(".js-one-acci");

    //없음을 눌렀을때
    acciAll.click(function(){
        var gap = $(this).val();

        if(gap =! ""){//체크가 됬다면
            unlock_check(acciOne);
        }
    });

    //사고중에 눌렀을때
    acciOne.click(function(){
        unlock_check(acciAll);
    });

    function unlock_check(who,who2){//해제하기 함수
        $(who).prop("checked", false);
    }

    //특수사고이력 없음 숨긴값과 동일하게 처리
    $('#special_accident_0').change(function(){
        var which = $(this).prop('checked');
        if (which == true) {
            $('#special_accident_3').prop("checked", true);
        }else{
            $('#special_accident_3').prop("checked", false);
        }
    });

    //사고횟수 0이면 피해금액 0
    $("select#accident_count").change(function(){
        var gap_count = $(this).val();
        if(gap_count == 0){
            $("select#accident_price_1, select#accident_price_2").val(0);
        }
    });

    //연식
    $("#progress2").validate({
        messages: {
            year_year:"출시년도를 선택해주세요.",
            year_month:"월을 선택해 주세요.",
        },
        errorPlacement: function(error, element) {
            if(element.attr("name") == "year_year"){
                error.insertAfter("#year_year");
            }else if(element.attr("name") == "year_month"){
                error.insertAfter("#year_month");
            }
        },
        submitHandler: function() {
            alert("완료")
        }
    });

    //연식 체크
    var yearSelect = $('.mycar-body.page-2 .select-v1');
    yearSelect.change(function() {
        var formBtn = $(this).parents('.mycar-body').find('.mycar-body__submit');
        var year = $('#year_year');
        var month = $('#year_month');
        if(year.val() != "" && month.val() != ""){
            formBtn.addClass('is-active').removeClass('is-disabled');
        }else{
            formBtn.removeClass('is-active').addClass('is-disabled');
        }
    });


    //사고이력
    $("#progress3").validate({
        messages: {
            special_accident:"특수사고를 선택해주세요."
        },
        errorPlacement: function(error, element) {
            error.insertAfter("#special_accident");
        },
        submitHandler: function() {
            alert("완료")
        }
    });

    //주행거리
    $("#progress4").validate({
        messages: {
            mileage:"주행거리를 입력해주세요."
        },
        errorPlacement: function(error, element) {
            if(element.attr("name") == "mileage"){
                error.insertAfter(".mycar-distance");
            }
        },
        submitHandler: function() {
            alert("완료")
        }
    });

    var mileageSelect = $('.mycar-body.page-4 .select-v1, .mycar-distance__input');
    mileageSelect.on('change keyup',function(){
        var mileage = $('#mileage').val();
        var mileageText = $('#mileageText').val();
        var formBtn = $(this).parents('.mycar-body').find('.mycar-body__submit');
        if(mileage != "" || mileageText != ""){
            formBtn.addClass('is-active').removeClass('is-disabled');
        }else{
            formBtn.removeClass('is-active').addClass('is-disabled');
        }
    })


    //옵션
    $("#progress5").validate({
        messages: {
            car_color:"색상을 선택해주세요."
        },
        errorPlacement: function(error, element) {
            error.insertAfter("#car_color");
        },
        submitHandler: function() {
            alert("완료")
        }
    });

    //옵션 체크
    var colorSelect = $('.mycar-body.page-5 .select-v1');
    colorSelect.on('change keyup',function(){
        var carColor = $('#car_color').val();
        var formBtn = $(this).parents('.mycar-body').find('.mycar-body__submit');
        if(carColor != ""){
            formBtn.addClass('is-active').removeClass('is-disabled');
        }else{
            formBtn.removeClass('is-active').addClass('is-disabled');
        }
    });

    $("input#mileageCheck").change(function(){
        var formBtn = $(this).parents('.mycar-body').find('.mycar-body__submit');
        var which = $(this).prop('checked');
        var $this = $(this).parents('.mycar-body__form');
        formBtn.removeClass('is-active').addClass('is-disabled');
        if (which == true) {
            $this.find('.select-v1').prop('required', false);
            $this.find('.select-v1').prop('disabled', true);
            $this.find('.select-v1').val('').addClass('is-disabled');
            $this.find('.mycar-distance__input').prop('required', true);
            $this.find('.mycar-distance__input').prop('readonly', false);
            $this.find('.mycar-distance').addClass('is-active')
        } else {
            $this.find('.select-v1').prop('required', true);
            $this.find('.select-v1').prop('disabled', false);
            $this.find('.select-v1').val('').removeClass('is-disabled');
            $this.find('.mycar-distance__input').prop('required', false);
            $this.find('.mycar-distance__input').prop('readonly', true);
            $this.find('.mycar-distance__input').val('');
            $this.find('.mycar-distance').removeClass('is-active');
        }
    });

    $("select#accident_price_1").change(function(){
       var gap_1 = $(this).val();
       var idx = $("option:selected",this).index();
       console.log(idx);

       $("select#accident_price_2").val(gap_1);
       $("select#accident_price_2 option:gt("+idx+")").show();
       $("select#accident_price_2 option:lt("+idx+")").hide();
   });

});
