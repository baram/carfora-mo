$(document).ready(function(){
    $('.tab-v1__link').on('click',function(e){
    	e.preventDefault();
    	var tabClass = $(this).attr('href');
    	$('.tab-v1__item, .tab-v1__content').removeClass('is-active');
    	$(this).parents('.tab-v1__item').addClass('is-active');
    	$("."+tabClass).addClass('is-active');
    });


    $('.js-align-input').on('click',function(e){
        e.preventDefault();
        $(this).parents('.bottom-select').fadeOut(500).find('.bottom-select__wrap').removeClass('is-active');
        var tmp = $('input[name="productAlign"]:radio:checked').val()
        $('.js-align-open').text(tmp);
    });

    $('.js-bottom-close').on('click',function(e){
        e.preventDefault();
        $(this).parents('.bottom-select').fadeOut(500).find('.bottom-select__wrap').removeClass('is-active');
    });

    $('.js-align-open').on('click',function(e){
        e.preventDefault();
        $('.bottom-select.type-align').fadeIn(300,function(){
            $(this).find('.bottom-select__wrap').addClass('is-active')
        });
    });

    // 바디스크롤 막기
    var bodyY;
    function scrollStop(){
        bodyY = $(window).scrollTop();
        console.log(bodyY)
    	$('html, body').addClass("no-scroll");
    	$('.common').css({"top":-bodyY,'position':'relative'});
    }
    function scrollStart(){
    	$('html, body').removeClass("no-scroll");
    	$('.common').css({'top':'auto','position':'static'});
    	bodyY = $('html,body').scrollTop(bodyY);
    }


    //히스토리
    $('.js-history-open').on('click',function(e){
        e.preventDefault();
        $('.history').addClass('is-active');
        scrollStop();
    });
    $('.history__back').on('click',function(e){
        e.preventDefault();
        $(this).parents('.history').removeClass('is-active');
        scrollStart();
    });


    function historyCheck(){
        var historyProduct = $(".js-history-check");
        var historyAll = $('#chkAll')
        historyAll.on('click',function(){
            if($(this).is(':checked')){
                historyProduct.prop("checked",true);
            }else{
                historyProduct.prop("checked",false);
            }
        });

        historyProduct.on('click',function(){
            var productChkNum = historyProduct.length;
            if(historyAll.is(':checked')){
                if($(".js-history-check:checked").length == productChkNum){
                    historyProduct.prop("checked",true);
                }else{
                    historyAll.prop("checked",false);
                }
            }
        });

        $('.js-history-check, #chkAll').change(function(){
            var isCheck = $('.js-history-check:checked').length;
            if(isCheck != 0){
                $('.history__scroll').find('.button-v2').removeClass('is-disabled').addClass('is-active');
            }else{
                $('.history__scroll').find('.button-v2').removeClass('is-active').addClass('is-disabled');
            }
        });

        $('.js-check-remove').on('click',function(e){
            e.preventDefault();
            $('.js-history-check:checked').parents('.product__item').remove();
            var isCheck = $('.js-history-check:checked').length;
            var productNum;
            if($('.history-product .product__item').hasClass('no-message')){
                productNum = $('.history-product .product__item').length -1;
            }else{
                productNum = $('.history-product .product__item').length;
            }
            if(productNum == 0){
                $('.history-count').hide();
				$('.no-message').removeClass('bld');
            }else{
                $('.history-count').text(productNum);
            }
            if(isCheck != 0){
                $('.history__scroll').find('.button-v2').removeClass('is-disabled').addClass('is-active');
            }else{
                $('.history__scroll').find('.button-v2').removeClass('is-active').addClass('is-disabled');
                $('#chkAll').prop("checked",false);
            }
        });
    }

    historyCheck();
    //하단 셀렉트 실행시 바깥클릭하면 닫기
    $(document).mouseup(function (e){
        var container = $('.bottom-select');
        if( container.has(e.target).length === 0){
            $('.bottom-select').fadeOut(500).find('.bottom-select__wrap').removeClass('is-active');
        }
    });
    //말줄임
    cellNoWrap($('.js-news-ellipsis'));
    function cellNoWrap(target) {
      target.css({'max-width':target.parent().width(),'white-space':'nowrap'},function(){});
    }
    $('.select-v1').change(function() {
        if($(this).val() !=""){
            $(this).addClass('is-check');
        }else{
            $(this).removeClass('is-check');
        }
    });
//검색 하얀창 뜨기 - 공통
    const searchOpen = document.querySelector('.js-search-open');
	const searchClose = document.querySelector('.js-search-close');
	const layerSearch = document.querySelector('.layer-search');
	const tagSearch = document.querySelector('.layer-search');
	const searchInput = document.querySelector('.search_box__auto-input');
	try{
		searchOpen.addEventListener("click",function(e){
            e.preventDefault();
			layerSearch.classList.add('is-active');
			searchInput.focus();
		});
		searchClose.addEventListener("click",function(){
			layerSearch.classList.remove('is-active');
		});

	}catch(e){}

    //placeholde
    $('.js-placeholder-style').focus(function(){
        $(this).parents('.placeholder-style').addClass('is-remove')
    }).blur(function(){
        var tmp = $(this).val().replace(/(^\s*)|(\s*$)/gi, "");
        if(tmp ==''){
            $(this).parents('.placeholder-style').removeClass('is-remove')
        }
    });

    //개별 페이지내 팝업 공통 동작
    $(".js-pop-open").off('click').on('click',function(e){
        e.preventDefault();
        $(".dim").show();
        $(".per-pop").fadeIn(400);
        scrollStop();
    });

    $(".js-pop-close").click(function () {
        $(".dim, .per-pop").fadeOut(200);
        scrollStart();
    });

});

    $(window).on("load resize", function () {
        var footerH = $('.footer').outerHeight();
        $('body').css('padding-bottom',footerH);
    });


$(document).ready(function(){
    
// //공통 인풋 스타일 에러 추가하기
//     $(".js-error-check").blur(function () {
//         var gap_input = $(this).val();
//         if(!gap_input){
//             $(this).addClass("error");
//         }else{
//             $(this).removeClass("error");
//         }
//     });


});//로드 영역
